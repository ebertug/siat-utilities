const { roundHalfUp, roundFloor } = require("./number");

const SALE_PRECISION = 2
const IVA = 0.13

class SiatValidations {
    constructor(sale, precision) {
        this.sale = sale
        this.precision = precision
    }

    static create(sale, precision) {
        return new SiatValidations(sale, precision)
    }

    subtotalWithDiscount(qty, unitPrice, totalDiscountAmount) {
        return qty * unitPrice - totalDiscountAmount
    }

    ivaAliquot(subtotalWithDiscount) {
        return roundHalfUp(subtotalWithDiscount * IVA, this.precision)
    }

    netSalePriceICE(subtotalWithDiscount, ivaAliquot) {
        return roundHalfUp(subtotalWithDiscount - ivaAliquot, this.precision)
    }
    
    totalDiscount(qty, discountAmount, discountPerUnit) {
        return discountAmount + (qty * discountPerUnit)
    }

    iceSpecificAmount(qty, iceSpecific) {
        return roundHalfUp(qty * iceSpecific, this.precision)
    }

    iceFactorAmount(netSalePriceICE, iceFactor) {
        return roundHalfUp(netSalePriceICE * iceFactor, this.precision)
    }

    subtotal(subtotalWithDiscount, iceSpecificAmount, iceFactorAmount) {
        return roundHalfUp(subtotalWithDiscount + iceSpecificAmount + iceFactorAmount, this.precision)
    }

    calculateDetail(detail) {
        const { 
            qty, 
            unitPrice, 
            iceFactor,
            iceSpecific,
            discountAmount,
            discountPerUnit
        } = detail

        const totalDiscountAmount = this.totalDiscount(qty, discountAmount, discountPerUnit)        
        const subtotalWithDiscount =this.subtotalWithDiscount(qty, unitPrice, totalDiscountAmount)
        const ivaAliquot = this.ivaAliquot(subtotalWithDiscount)
        const netSalePriceICE = this.netSalePriceICE(subtotalWithDiscount, ivaAliquot)

        const iceSpecificAmount = this.iceSpecificAmount(qty, iceSpecific)
        const iceFactorAmount = this.iceFactorAmount(netSalePriceICE, iceFactor)
        const subtotal = this.subtotal(subtotalWithDiscount, iceSpecificAmount, iceFactorAmount)

        return {
            ...detail,
            totalDiscountAmount,
            ivaAliquot,
            netSalePriceICE,
            iceSpecificAmount,
            iceFactorAmount,
            subtotal
        }
    }

    calculateSaleTotal(sale, saleDetails) {
        const { subtotalAmount,  iceSpecificAmount, iceFactorAmount } = saleDetails.reduce((acc, item) => {
            acc.subtotalAmount = (acc.subtotalAmount || 0) + item.subtotal
            acc.iceSpecificAmount = (acc.iceSpecificAmount || 0) + item.iceSpecificAmount
            acc.iceFactorAmount = (acc.iceFactorAmount || 0) + item.iceFactorAmount
            return acc
        }, {})

        const totalAmount = roundHalfUp(subtotalAmount - sale.additionalDiscount, SALE_PRECISION)

        return {
            totalAmount,
            iceSpecificAmount: roundHalfUp(iceSpecificAmount, SALE_PRECISION),
            iceFactorAmount: roundHalfUp(iceFactorAmount, SALE_PRECISION),
            totalAmountSubjectToIva: roundHalfUp(totalAmount - iceSpecificAmount - iceFactorAmount - sale.couponAmount, SALE_PRECISION),
            additionalDiscount: sale.additionalDiscount,
        }
    }

    calculateSaleIce() {
        const saleDetails = this.sale.saleDetails.map(detail => this.calculateDetail(detail))      
        const saleTotals = this.calculateSaleTotal(this.sale, saleDetails)

        return {
            ...saleTotals,
            saleDetails
        }
    }
}

module.exports = { SiatValidations }