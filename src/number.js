const numeral = require('numeral');

function roundHalfUp(n, decimals=0){
    let multiplier = 10**decimals;
    return Math.round(numeral(n).multiply(multiplier).value()) / multiplier;
}

module.exports.roundHalfUp = roundHalfUp;

function roundFloor(n, decimals=0) {
    let multiplier = 10**decimals;
    return Math.floor(numeral(n).multiply(multiplier).value()) / multiplier;
}

module.exports.roundFloor = roundFloor; 