

### ALCANZADA POR 

Validación de documento sector [ICE](https://siatinfo.impuestos.gob.bo/index.php/facturacion-en-linea/archivos-xml-xsd-de-facturas-electronicas/validaciones-documentos-sector/validaciones)

```
Operaciones

montoTotal=Σ(subTotal)−descuentoAdicional	 
montoIceEspecifico=Σ(montoIceEspecifico)	 
montoIcePorcentual=Σ(montoIcePorcentual)	 
montoTotalMoneda=montoTotal /tipoCambio	 
montoTotalSujetoIva=montoTotal−montoIceEspecifico−montoIcePorcent	 
montoIceEspecifico=cantidadIce× alicuotaEspecifica	 
montoIcePorcentual=precioNetoVentaIce ×alicuotaPorcentual

Donde:
alicuotaIva=(cantidad∗precioUnitario-descuento)∗0.13
precioNetoVentaIce=(cantidad × precioUnitario - descuento)−alicuotaIva
subTotal=((cantidad * precioUnitario)- montoDescuento) + montoIcePorcentual + montoIceEspecifico
 ```

#### Example

 ```
 const sale = {
    couponAmount: 0,
    additionalDiscount: 0,
    saleDetails: [
        {
            description: 'CERVEZA PALE ALE - MATILDA - 330 ML',
            qty: 6.0000, 
            unitPrice: 9.62, 
            iceFactor: 0.01,
            iceSpecific: 1.2969,
            discountAmount: 0,
            discountPerUnit: 0
        },
        {
            description: 'CERVEZA PREMIUM NEGRA - BUDDA - 330 ML',
            qty: 6.0000, 
            unitPrice: 10.61, 
            iceFactor: 0.01,
            iceSpecific: 1.2969,
            discountAmount: 0,
            discountPerUnit: 0
        },
        {
            description: 'CERVEZA IPA - MATILDA - 330 ML',
            qty: 6.0000,
            unitPrice: 12.35, 
            iceFactor: 0.01,
            iceSpecific: 1.2969,
            discountAmount: 0,
            discountPerUnit: 0
        }
    ]
}

const validation = SiatValidations.create(sale, 2)
console.log(validation.calculateSaleIce()) 
```
#### Result
```
{
  totalAmount: 220.51,
  iceSpecificAmount: 23.34,
  iceFactorAmount: 1.69,
  totalAmountSubjectToIva: 195.48,
  additionalDiscount: 0,
  saleDetails: [
    {
      description: 'CERVEZA PALE ALE - MATILDA - 330 ML',
      qty: 6,
      unitPrice: 9.62,
      iceFactor: 0.01,
      iceSpecific: 1.2969,
      discountAmount: 0,
      discountPerUnit: 0,
      totalDiscountAmount: 0,
      ivaAliquot: 7.5,
      netSalePriceICE: 50.22,
      iceSpecificAmount: 7.78,
      iceFactorAmount: 0.5,
      subtotal: 66
    },
    {
      description: 'CERVEZA PREMIUM NEGRA - BUDDA - 330 ML',
      qty: 6,
      unitPrice: 10.61,
      iceFactor: 0.01,
      iceSpecific: 1.2969,
      discountAmount: 0,
      discountPerUnit: 0,
      totalDiscountAmount: 0,
      ivaAliquot: 8.28,
      netSalePriceICE: 55.38,
      iceSpecificAmount: 7.78,
      iceFactorAmount: 0.55,
      subtotal: 71.99
    },
    {
      description: 'CERVEZA IPA - MATILDA - 330 ML',
      qty: 6,
      unitPrice: 12.35,
      iceFactor: 0.01,
      iceSpecific: 1.2969,
      discountAmount: 0,
      discountPerUnit: 0,
      totalDiscountAmount: 0,
      ivaAliquot: 9.63,
      netSalePriceICE: 64.47,
      iceSpecificAmount: 7.78,
      iceFactorAmount: 0.64,
      subtotal: 82.52
    }
  ]
}
```